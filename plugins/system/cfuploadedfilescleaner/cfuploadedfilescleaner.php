<?php

/**
 * @package         Convert Forms
 * @version         2.7.1 Pro
 * 
 * @author          Tassos Marinos <info@tassos.gr>
 * @link            http://www.tassos.gr
 * @copyright       Copyright © 2020 Tassos Marinos All Rights Reserved
 * @license         GNU GPLv3 <http://www.gnu.org/licenses/gpl.html> or later
*/

defined('_JEXEC') or die('Restricted access');

jimport('joomla.filesystem.file');
jimport('joomla.filesystem.folder');

use Joomla\Registry\Registry;

class PlgSystemCFUploadedFilesCleaner extends JPlugin
{
    /**
     *  Application Object
     *
     *  @var  object
     */
    protected $app;

    /**
     * Remove old uploaded files based on the preferences set in each File Upload Field.
     *
     * @return void
     */
    public function onConvertFormsCronTask($task, $options = array())
    {
		if ($task != 'uploadedfilescleaner')
		{
			return;
        }

        $this->log('Starting Uploaded Folders Cleaner - Time Limit: ' . $options['time_limit'] . 's');
        
        // Start the clock!
        $clockStart = microtime(true);
        
        // Load all forms
        $upload_folders = $this->getUploadFolders();

        if (!$upload_folders || !is_array($upload_folders) || empty($upload_folders))
        {
            $this->log('No upload folders found that need cleaning. Abort.');
            return;
        }

        foreach ($upload_folders as $key => $folder)
        {
            $auto_delete_files = $folder['auto_delete_files'];

            $this->log("Processing folder " . $folder['path']);
            $this->log("Searching for files older than $auto_delete_files days");
            
            $exclude_files = [
                '.htacces',
                'index.html'
            ];

            $files_to_delete = \JFolder::files($folder['path'], '.', false, true, $exclude_files);
            $processedCount = 0;

            foreach ($files_to_delete as $key => $file)
            {
                $diff_in_miliseconds = time() - filemtime($file);

                // Skip the file if it's not old enough
                if ($diff_in_miliseconds < (60 * 60 * 24 * $auto_delete_files))
                {
                    continue;
                }

                // OK, the file is old enough. We can now delete it.
                if (\JFile::delete($file))
                {
                    $processedCount++;
                
                    // Timeout check -- Only if we did delete a file!
                    $clockNow = microtime(true);
                    $elapsed  = $clockNow - $clockStart;

                    if (($options['time_limit'] > 0) && ($elapsed > $options['time_limit']))
                    {
                        $leftOvers = count($files_to_delete) - $processedCount;
                        $this->log("I ran out of time. Number of files in queue left unprocessed: $leftOvers");
                        return;
                    }
                }
            }

            $this->log("$processedCount files deleted");
        }

        $this->log('Uploaded Folders Cleaner Stopped');
    }

    /**
	 *  Get an array of all Upload Folders used by forms
	 *
	 *  @return  array
	 */
	private function getUploadFolders()
	{
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		$query
			->select('*')
			->from('#__convertforms');

		$db->setQuery($query);

        $forms = $db->loadObjectList();

        if (!is_array($forms) || empty($forms))
        {
            return;
        }

        $upload_folders = [];
        
        foreach ($forms as $key => $form)
        {
            $params = json_decode($form->params);            

            if (!isset($params->fields))
            {
                continue;
            }

            foreach ($params->fields as $key => $field)
            {
                $field = new Registry($field);

                if ($field->get('type') !== 'fileupload')
                {
                    continue;
                }

                $auto_delete_files = (int) $field->get('auto_delete_files', 0);

                if ($auto_delete_files == 0)
                {
                    continue;
                }

                $info = [
                    'path' => $field->get('upload_folder'),
                    'auto_delete_files' => $auto_delete_files
                ];

                $upload_folders[] = $info;
            }
        }

        return $upload_folders;
    }
    
    /**
     * Log debug message to lg file
     *
     * @param  strng $msg
     *
     * @return void
     */
    private function log($msg)
    {
        try {
            \JLog::add($msg, \JLog::DEBUG, 'convertforms.cron.uploadedfilescleaner');
        } catch (\Throwable $th) {
        }
    }
}

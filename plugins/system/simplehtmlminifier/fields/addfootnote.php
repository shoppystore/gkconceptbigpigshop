<?php

/* Simple HTML Minifier extension for Joomla!
--------------------------------------------------------------
 Copyright (C) 2016 AddonDev. All rights reserved.
 Website: https://addondev.com
 GitHub: https://github.com/addondev
 Developer: Philip Sorokin
 Location: Russia, Moscow
 E-mail: philip.sorokin@gmail.com
 Created: January 2016
 License: GNU GPLv2 http://www.gnu.org/licenses/gpl-2.0.html
--------------------------------------------------------------- */

defined('_JEXEC') or die;

class JFormFieldAddfootnote extends JFormField
{	
	protected $type = 'addfootnote';
	
	protected function getInput()
	{
		$doc = JFactory::getDocument();
		
		$doc->addStyleDeclaration("
			.add-spacer, .add-spacer label {
				text-transform: uppercase !important;
				font-weight: bold !important;
				cursor: default !important;
			}
			#general .control-group:last-child .controls {
				margin: 0 !important;
			}
			#general .control-group:last-child .control-label {
				display: none !important;
			}
			hr {
				clear: both;
				float: none;
			}
			@media (min-width: 600px) {
				#myTabContent .add-indent {text-indent: 20px}
			}
		");
		
		$doc->addScriptDeclaration("
			(function(w, d)
			{
				var addLsn = d.addEventListener ? function(el, e, h)
				{
					el.addEventListener(e, h, false);
				} 
				: function(el, h, e)
				{
					if(h === 'DOMContentLoaded'){el = w; h = 'load';}
					el.attachEvent('on' + e, function(e){h.call(el, e)});
				};
				
				addLsn(d, 'DOMContentLoaded', function(e)
				{
					var txt = d.getElementById('jform_params_exclude_tags');
					
					addLsn(txt, 'blur', function(e)
					{
						this.value = this.value.replace(/[^a-zA-Z0-9,-]/g, '');
						this.value = this.value.replace(/^[,-]+|[,-]+$/g, '');
						this.value = this.value.replace(/([,-]){2,}/g, '$1');
						this.value = this.value.replace(/[,]/g, ', ');
					});
				});

			})(window, document);
		");
		
		return '<hr/>' . JText::_('PLG_SIMPLEHTMLMINIFIER_REVIEW');
	}
	
}

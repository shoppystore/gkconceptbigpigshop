<?php
global $done_irenames;
global $done_trenames;

if(!isset($done_irenames))
	$done_irenames = array();
if(!isset($done_trenames))
	$done_trenames = array();


if(isset($SETTINGS->image_renamer)){
	
	if($SETTINGS->image_renamer && (!empty($image_renamer_index) || !empty($thumb_renamer_index))){	
		
		if($SETTINGS->image_renamer->use_in_import){
			
			$products_images = null;
			for($IN = 0 ; $IN < $SETTINGS->image_renamer->images; $IN++){
				
				$images_path = JPATH_SITE. DS . 'images' . DS . 'stories' . DS . 'virtuemart' . DS . 'product'. DS;
				$thumbs_path = JPATH_SITE. DS . 'images' . DS . 'stories' . DS . 'virtuemart' . DS . 'product'. DS . 'resized'. DS;
				$name = "";
				
				if(isset($image_renamer_index["image".($IN + 1)."name"])){
					
					$csv_index = $image_renamer_index["image".($IN + 1)."name"];
					
					$newname = $data[$csv_index];
						
					if(!isset($done_irenames[$newname])){
							
						
						if($products_images == null){
							$products_images = ir_loadProductImages($db,$id);
						}
						
						if(count($products_images) > $IN){
							
							$oldname  = basename($products_images[$IN]->file_url);
							$old_path = $images_path . $oldname;
							$new_path = $images_path . $newname;
							
							if($oldname){
								
								if(strcasecmp( $oldname , $newname) !== 0){
									if(!$newname){
										
										$db->setQuery("DELETE FROM #__virtuemart_product_medias WHERE virtuemart_media_id = " . $products_images[$IN]->virtuemart_media_id);
										$db->query();
										
										$db->setQuery("SELECT count(virtuemart_media_id) FROM #__virtuemart_product_medias WHERE virtuemart_media_id = " . $products_images[$IN]->virtuemart_media_id);
										
										$count = $db->loadResult();
										if(!$count)
											$count = 0;
										
										if($count < 2){
											$db->setQuery("DELETE FROM #__virtuemart_medias WHERE virtuemart_media_id = " . $products_images[$IN]->virtuemart_media_id);
											$db->query();		
										}
										
									}else{
										
										
										if(!ir_fileRealyExists($old_path)){
											
											$name = "";
											
											if(ir_fileRealyExists($new_path)){
												
												$name       = $newname;
												$new_t_name = $newname;
												
												if(!ir_fileRealyExists($images_path . 'resized'. DS .$new_t_name)){
													$new_t_name = $products_images[$IN]->file_url_thumb;
												}
												global $media_product_path;
												if($SETTINGS->image_renamer->thumbs_names){
													ir_updateMediaByCondition($db, "file_url LIKE " . $db->quote("$media_product_path/" . $oldname) . ' ' , $newname, NULL);
												}else{
													ir_updateMediaByCondition($db, "file_url LIKE " . $db->quote("$media_product_path/" . $oldname) . ' ' , $newname, $new_t_name);
												}
												
											}else{
												
												$db->setQuery("DELETE FROM #__virtuemart_product_medias WHERE virtuemart_media_id = " . $products_images[$IN]->virtuemart_media_id);
												$db->query();
												
												$db->setQuery("SELECT count(virtuemart_media_id) FROM #__virtuemart_product_medias WHERE virtuemart_media_id = " . $products_images[$IN]->virtuemart_media_id);
								
												$count = $db->loadResult();
												if(!$count)
													$count = 0;
												
												if($count < 2){
													$db->setQuery("DELETE FROM #__virtuemart_medias WHERE virtuemart_media_id = " . $products_images[$IN]->virtuemart_media_id);
													$db->query();		
												}
											}
											
										}else{
											
											$ok      = false;
											$existed = false;
											
											if($SETTINGS->image_renamer->overwrite){
												
												$ok = rename($old_path,$new_path); 
											}else{
												if(ir_fileRealyExists($new_path)){
													
													$ok = true;
													$existed = true;
													
												}else{
													
													$ok = rename($old_path,$new_path); 
													
												}
											}
											
											if(!$existed)
												$done_irenames[$newname] = 1;
											
											if($ok){
												
												$name = $newname;
												$old_t_name = basename($products_images[$IN]->file_url_thumb);
												$new_t_name = $newname;
												
												$ok_thumb = false;
												
												if($old_t_name && $new_t_name){
													if(ir_fileRealyExists($images_path . 'resized'. DS .$old_t_name)){
														
														if($SETTINGS->image_renamer->overwrite){
															
															$ok_thumb = rename($images_path . 'resized'. DS .$old_t_name, $images_path . 'resized'. DS .$new_t_name);
															
														} else {
															
															if(ir_fileRealyExists($images_path . 'resized'. DS .$new_t_name)){
																$ok_thumb = true;
															}else
																$ok_thumb = rename($images_path . 'resized'. DS .$old_t_name, $images_path . 'resized'. DS .$new_t_name);
														}
													
														if(!$ok_thumb){
															$new_t_name = $old_t_name;
														}
													}
												}else if($new_t_name){
													if(ir_fileRealyExists($images_path . 'resized'. DS .$new_t_name))
														$ok_thumb = true;
												}
												
												if($ok){
													if($existed){
														
														if($SETTINGS->image_renamer->thumbs_names){
															ir_updateMedia($db, $products_images[$IN]->virtuemart_media_id, $name, NULL);
														}else{
															ir_updateMedia($db, $products_images[$IN]->virtuemart_media_id, $name, $new_t_name);
														}
														
														
																	 
													}else{
														global $media_product_path;
														if($SETTINGS->image_renamer->thumbs_names){
															ir_updateMediaByCondition($db, "file_url LIKE " . $db->quote("$media_product_path/" . $oldname) . ' ' , $name, NULL);
														}else{
															ir_updateMediaByCondition($db, "file_url LIKE " . $db->quote("$media_product_path/" . $oldname) . ' ' , $name, $new_t_name);
														}
													}
												}
												
											}else{
												
												$name = $oldname;
											}
										}
									}									
								}
							}else{
								$name = "";
							}
						}else{
							
							//insert
							$name = "";
							
							if($newname){
								$new_path = $images_path . $newname;
								if(ir_fileRealyExists($new_path)){
								   $name = $newname;
								   
								   $thumb_path = $images_path . 'resized'. DS . $newname;
								   $thumb_name = "";
								   
								   if(ir_fileRealyExists($thumb_path))
									   $thumb_name = $newname;
								   
								   $mid = ir_createMedia($db, $user->id, $newname ,$thumb_name);	
								   if($mid){
										ir_changeProductMedia($db, $id , NULL , $mid, $IN + 1);
										$products_images = ir_loadProductImages($db,$id);
								   }
								}
							}
						}
						
					}
				}
				
				//thumbs
				if($SETTINGS->image_renamer->thumbs_names){
					
					
					if(isset($thumb_renamer_index["thumb".($IN + 1)."name"])){
						
						$csv_index = $thumb_renamer_index["thumb".($IN + 1)."name"];
						$newname = $data[$csv_index];
						if(!isset($done_trenames[$newname])){
							
							$done_trenames[$newname] = 1;
							if($products_images == null){
								$products_images = ir_loadProductImages($db,$id);
							}
							
							if(count($products_images) > $IN){
							
								$oldname  = basename($products_images[$IN]->file_url_thumb);
								if(is_numeric($oldname))
									$oldname = "";
							
								$old_path = $thumbs_path . $oldname;
								$new_path = $thumbs_path . $newname;
								
								if(strcasecmp( $oldname , $newname) !== 0){
									$name = "";	
								
									if($oldname && $newname){
										
										if(ir_fileRealyExists($old_path) && !ir_fileRealyExists($new_path)){
											if(rename($old_path,$new_path)){
												$name = $newname;
											}
										}else if(ir_fileRealyExists($old_path) && ir_fileRealyExists($new_path)){
											if($SETTINGS->image_renamer->overwrite){
												unlink($new_path);
												if(rename($old_path,$new_path)){
													$name = $newname;
												}
											}else{
												$name = $newname;
											}
										}else if(!ir_fileRealyExists($old_path) && ir_fileRealyExists($new_path)){
											$name = $newname;
										}
									}else if($newname){
										if(ir_fileRealyExists($new_path)){
											$name = $newname;
										}
									}else if(!$newname){
										$count = ir_getMediaCount($db, $products_images[$IN]->virtuemart_media_id);
										if($count > 1){
											$mid = ir_createMediaDuplicate($db, $products_images[$IN]->virtuemart_media_id);
											if($mid){
												if(ir_changeProductMedia($db, $id ,$products_images[$IN]->virtuemart_media_id ,$mid,$IN + 1)){
													$products_images[$IN]->virtuemart_media_id = $mid;
												}
												ir_updateMedia($db, $mid, NULL, '');
												$products_images = ir_loadProductImages($db,$id);
											}
										}else if($count == 1){
											ir_updateMedia($db, $products_images[$IN]->virtuemart_media_id, NULL, '');
										}
									}
									
									if($name){
										$count = ir_getMediaCount($db, $products_images[$IN]->virtuemart_media_id);
										if($count > 1){
											$mid = ir_createMediaDuplicate($db, $products_images[$IN]->virtuemart_media_id);
											if($mid){
												if(ir_changeProductMedia($db, $id ,$products_images[$IN]->virtuemart_media_id , $mid ,$IN + 1)){
													$products_images[$IN]->virtuemart_media_id = $mid;
												}
												ir_updateMedia($db, $mid, NULL, $name);
												$products_images = ir_loadProductImages($db,$id);
											}
										}else if($count == 1){
											ir_updateMedia($db, $products_images[$IN]->virtuemart_media_id, NULL, $name );
										}
									}
									
								}
							}
						}
					}
				}
			}
		}	
	}
}



?>

<?php
	
	use Joomla\CMS\Factory;
	
	$document = Factory::getDocument();
	$document->addStyleSheet('modules/mod_bigpig_good_ideas/assets/css/style.min.css');
	$document->addScript('modules/mod_bigpig_good_ideas/assets/css/masonry.min.js');
 
 
?>

<!--<div class="masonry-wrapper" style="min-height: 50rem">-->
<div style="min-height: 30rem">
    <div class="masonry" id="goodideas">
	    <?php
		    foreach ($defaultbigpig as $item) {
			    foreach ($item->img as $ite) {
				    if (is_file($ite['src']))
				    {
					    ?>
                        <a href="<?php echo $item->link ?>" target="_blank">
                            <div class="masonry-item">
                                <div class="masonry-content">
                                    <img src="<?php echo $ite['src'] ?>" alt="<?php echo $item->title ?>">
<!--                                    <h6 class="masonry-title">--><?php //echo $item->title ?><!--</h6>-->
<!--                                    <p class="masonry-description">--><?php //echo  strlen($item->introtext) > 100 ? substr($item->introtext,0,100)."..." : $item->introtext; ?><!--</p>-->
                                </div>
                            </div>
                        </a>
					    <?php
				    }
			    }
		    }
	    ?>
     
    </div>
    <div class=" text-center">
        <button type="submit" id="loadmoregoodideas" value="1" class="btn btn-primary">Load More</button>
    </div>
</div>
<style>

	#loadmoregoodideas {
		padding: 10px;
		text-align: center;
		box-shadow: 0 1px 1px #ccc;
		transition: all 600ms ease-in-out;
		-webkit-transition: all 600ms ease-in-out;
		-moz-transition: all 600ms ease-in-out;
		-o-transition: all 600ms ease-in-out;
	}

</style>

<script>
	jQuery(function ($) {

		$(document).on('click', '#loadmoregoodideas', function () {
			var data = {time: parseInt($('#loadmoregoodideas').val())};
			$.ajax({
				url: 'index.php?option=com_ajax&module=bigpig_good_ideas&method=linkImages&format=json',
				type: "POST",
				data: data,
				success: function (response) {
					var item = JSON.parse(JSON.stringify(response))
                    console.log(item.data);
					if (item.data == undefined || item.data.length < 1) {
						$('#loadmoregoodideas').hide();
					} else {
						$('#goodideas').append(item.data);
						$('#loadmoregoodideas').val(parseInt($('#loadmoregoodideas').val()) + 1);
					}

				}
			});

		});

	});
</script>

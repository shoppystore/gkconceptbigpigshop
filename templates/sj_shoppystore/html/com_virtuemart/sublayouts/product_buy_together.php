<?php
	/**
	 *
	 * Show the product_buy_together
	 * @author BigPig
	 */
	// Check to ensure this file is included in Joomla!
	defined('_JEXEC') or die('Restricted access');
	$product = $viewData['product'];
	$currency = $viewData['currency'];
	if (empty($product->customfieldsSorted['related_products'])) {
		return;
	}
?>
<div class="iq-card center">
    <div class="row text-center">
        <h3 class="card-title">Sản phẩm thường được mua cùng : </h3>
    </div>
    <div class="iq-card-body">
		<?php
			$i = 1;
			foreach ($product->customfieldsSorted['related_products'] as $fied) {
				?>
                <ul class="banner banner-slth1">
                    <li><a href="<?= $fied->infor->link ?>"></a>
						<?php echo $fied->display ?>
                    </li>
                </ul>
                <style>
                    .vm-img-desc {
                        display: none;
                    }
                </style>
				<?php
				if ($i < count($product->customfieldsSorted['related_products'])) {
					?>
                    <i class="fa fa-plus pl-2 pr-2 pt-5" style="height: 100px;float: left;" aria-hidden="true"></i>
					<?php
				}
				
				if ($i == 3) {
					break;
				}
				
				$i++;
			}
		?>


        <table class="table table-hover">
            <tbody>
			<?php
				$sum = 0;
				$i = 1;
				$currencyDisplay = CurrencyDisplay::getInstance();
				foreach ($product->customfieldsSorted['related_products'] as $fied) {
					$sum += $fied->infor->allPrices['0']['salesPrice'];
					?>
                    <tr>
                        <td><?php echo $fied->product_name ?></td>
                        <td></td>
                        <td><?php echo $currencyDisplay->priceDisplay($fied->infor->allPrices['0']['salesPrice']); ?></td>
                    </tr>
					<?php
					if ($i == 3) {
						break;
					}
					
					$i++;
				}
			?>
            <tr>
                <td colspan="2"></td>
                <td><?php echo $currencyDisplay->priceDisplay($sum); ?></td>
            </tr>
            </tbody>
        </table>


    </div>
</div>
<div class="clear"></div>